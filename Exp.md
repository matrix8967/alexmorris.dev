---
layout: post
title: Experience
description: ~$ history | grep "work" > Work_Experience.txt
image: assets/images/Upgrade.jpg
nav-menu: true
show_tile: true
---

> Rainbow Road.

### DevOps:

-   Infrastructure as Code (IaC) using Ansible and Terraform.
-   Change Management & Source Control using Git. (Gitlab/Github Enterprise.)
-   CI/CD Pipelines and Automation.
-   Configured and maintained cloud infrastructure for Production Applications with thousands of daily active users.
-   Maintained VPC Network services such as Reverse Proxies and VPNs.
-   Worked with Engineering & Operations teams to create usable and secure hybrid cloud services.
-   Built Custom Certificate software allowing faster go-live time for clients with automatic creation & renewal.

### Network & Systems Engineering:

-   Designed & implemented a 10-Gigabit Fiber Ring WAN Network between Tulsa & Dallas.
-   Built a state-of-the-art Wireless LAN, from planning through installation.
-   Designed & deployed an Enterprise VPN using Wireguard & Caddy.
-   Used Ansible to transition legacy infrastructure into version controlled code.
-   Build a Enterprise NAS to facilitate a Data Lake for media departments.
-   Replaced aging VMWare & Hyper-V Infrastructure with ProxMox cluster.
-   Supported 200+ employees and their varying hardware while onsite and remote.
-   Managed Linux, MacOS, and Windows user devices using Enterprise MDMs. (O365, Active Directory, JAMF, GSuite, etc.)
-   User training & Docs that are well written, bite-sized, and _actionable_.

### NetSec:

-   Forensic Imaging, Recovery, Incident Reseponse and Data Assurance.
-   Developed workflows, tools, and services to enforce compliance for SOC2.
-   Created a "Security Culture" hosted Q&A's, Docs, and Demos to make security a common conversation topic.
-   Developed Purple Team Security proceedures to move security measures earlier in the build process; baking security in instead of bolting on.
-   Logging, Monitoring, and Visibility.

---

### Volunteer & Activism:

-   Published in the Q1 2020 issue of [2600: The Hacker Quarterly](https://store.2600.com/products/winter-2019-2020) on Student Privacy issues.
-   Featured in the [EFF's case study on Student Privacy](https://www.eff.org/deeplinks/2017/03/privacy-practice-not-just-policy-system-administrator-advocating-student-privacy).
-   Spearheaded After-School Cyber-Security Club & Taught NetSec Fundamentals to Students.
-   Jumping in feet-first to organize and triage [wayward firmware updates](https://github.com/system76/firmware-open/issues/98) on a holiday weekends.
-   I also got an [award](assets/images/Award.png) from my peers for being a super nice dude.

### Personal Projects:

-   Documenting my personal [projects](https://text.mainframe.computer/2020/07/23/This-Machine.html) [notes,](https://grimoire.heretic.dev/Configs/Terminal/zshrc.html) and [research,](https://grimoire.heretic.dev/Notes/Ansible/Ansible_Networking_Links.html) in a public journal.
-   ✅️ ~~Setup my own Matrix.org Federated chat server with Jitsi Video Conferencing.~~ [Done!](https://element.heretic.dev/#/welcome)
-   A never ending list of [consoles,](https://mastodon.social/@matrix8967/105506010036233503) [phones,](https://mastodon.social/@matrix8967/105506005559605437) and [other](https://mastodon.social/@matrix8967/104379093685416474) [devices](https://mastodon.social/@matrix8967/104990241961700874) to [hack.](https://mastodon.social/@matrix8967/103377713638351769)
-   Hosting [Virtual Table Top](assets/images/VTT.png), [start](assets/images/Start_Heretic.png) [pages](assets/images/Start_Mainframe.png), [secure paste](assets/images/Paste.png) and [file](assets/images/Send.png) services.
-   Doing more with [less.](https://mastodon.social/@matrix8967/107206499853243764)
-   Started working on a 2nd [FOSS Game in the GoDot Engine.](https://mastodon.social/@matrix8967/103746307861989982)
-   ✅️ ~~Setting up better insights into my LAN, [such as Grafana](https://mastodon.social/@matrix8967/103640872967140961), ELK, and Graylog.~~
-   ✅️ ~~Setup my own NextCloud server to host my own Cloud Storage and Productivity Suite.~~

---

### git blame IRL:

When I graduated high school in 2007 I said I’d never go back even if they paid me. Shortly afterwards, I was offered a job at the same high school and I went back without question.

I continued working in K-12 schools for a decade. I focused on Automation, Virtualization, and Security using Open Source Tools. K12 provided me with ample opportunities to give back to the community and invest in students the way so many had invested in me during my life.

In 2018 I left K12 to work at an EdTech Startup in Little Rock as a DevOps Engineer and eventually became the organizations Subject Matter Expert in Networking & Security. After a sabattical - I returned to the Enterprise as a Systems Engineer for a large financial institution.

As of June 2023 - I'm a DevOps Engineer for ALTR, Inc.
