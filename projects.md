---
layout: post
title: Code
description: Digital macaroni art.
image: assets/images/Seg_Fault1.jpg
nav-menu: true
show_tile: true
---

<a href="https://gitlab.com/newyearstudios/ritual-jam">Segmentation_Fault</a> is a game I made with <a href="https://www.newyearstudios.com/">newyear studios.</a> This was our first title to work on together, and my first time doing any start-to-finish programmatic work, as well as my first published work of fiction. I learned a lot through the crucible of troubleshooting, and hindsight. Feel free to fork it, clone it, or do what you like. Feedback and PRs welcome.

<a href="https://gitlab.com/matrix8967/alexmorris.dev">alexmorris.dev</a> Whoa recursion! 🤯 You can see all the code, <a href="https://gitlab.com/matrix8967/alexmorris.dev/commits/master"> (Signed) Commits</a>, and <a href="https://gitlab.com/matrix8967/alexmorris.dev/-/jobs">Watch the CI/CD jobs in real time</a> on Gitlab. 👍

<a href="https://gitlab.com/matrix8967/scrolls">Scrolls</a> is the repo for dotfiles, configs, scripts, etc.

<a href="https://gitlab.com/matrix8967/gutter_bonez">Gutter_Bonez</a> is my _Ansible / Infra as Code_ repo.

<a href="https://text.mainframe.computer/">text.mainframe.computer</a> This is where I put really biased opinions, virtue signaling, and digital harm-reduction. <a href="https://gitlab.com/matrix8967/text-mainframe-computer/">Repo 🔗️.</a>

<a href="https://grimoire.heretic.dev/">Grimoire.heretic.dev</a> (つ•̀ᴥ•́)つ*:･ﾟ✧ My book of incantations.<a href="https://gitlab.com/matrix8967/grimoire"> Repo 🔗️. </a>

<a href="https://start.mainframe.computer/">start.mainframe.computer</a> This is a start page, mostly used for testing endpoints. <a href="https://gitlab.com/matrix8967/start-mainframe-computer">Repo 🔗️.</a>

<a href="https://start.heretic.dev/">start.heretic.dev</a> This is a start page, mostly used for testing endpoints. <a href="https://gitlab.com/matrix8967/morning-star">Repo 🔗️.</a>
